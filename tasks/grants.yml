---
- name: MySQL | Test whether the root password has already been changed (external hostname).
  mysql_user:
    user: root
    host: "{{item}}"
    password: "{{galera_dbusers['root']['password']}}"
    login_user: root
    login_password: "{{galera_dbusers['root']['password']}}"
    priv: "*.*:ALL,GRANT"
  with_items:
   - "{{ansible_hostname}}"
   - 127.0.0.1
   - ::1
   - localhost
  register: root_pw_already_set
  ignore_errors: yes
  when: ansible_hostname != 'localhost'
  tags:
    - change_root_password

- name: MySQL | Changing the root password (external hostname).
  mysql_user:
    user: root
    host: "{{item}}"
    password: "{{galera_dbusers['root']['password']}}"
    login_user: root
    login_password: '' 
    priv: "*.*:ALL,GRANT"
  with_items:
   - "{{ansible_hostname}}"
   - 127.0.0.1
   - ::1
   - localhost
  when: "root_pw_already_set|failed and ansible_hostname != 'localhost'"
  tags:
    - change_root_password

- name: MySQL | Test whether the root password has already been changed (local hostname).
  mysql_user:
    user: root
    host: "{{item}}"
    password: "{{galera_dbusers['root']['password']}}"
    login_user: root
    login_password: "{{galera_dbusers['root']['password']}}"
    priv: "*.*:ALL,GRANT"
  with_items:
   - 127.0.0.1
   - ::1
   - localhost
  register: root_pw_already_set
  ignore_errors: yes
  when: ansible_hostname == 'localhost'
  tags:
    - change_root_password

- name: MySQL | Changing the root password (local hostname).
  mysql_user:
    user: root
    host: "{{item}}"
    password: "{{galera_dbusers['root']['password']}}"
    login_user: root
    login_password: ''
    priv: "*.*:ALL,GRANT"
  with_items:
   - 127.0.0.1
   - ::1
   - localhost
  when: "root_pw_already_set|failed and ansible_hostname == 'localhost'"
  tags:
    - change_root_password

- name: MySQL | Configure MySql for easy access as root user
  template:
    src: root_dot_my.cnf.j2
    dest: /root/.my.cnf
    owner: root
    group: root
    mode: 0600

- name: MySQL | Remove anonymous MySQL server user
  mysql_user:
    name: ""
    host: "{{item}}"
    state: absent
  with_items:
    - "{{ansible_hostname}}"
    - localhost

- name: MySQL | Remove the MySQL test database
  mysql_db:
    name: test
    state: absent

- name: Add xtrabackup database user (for Galera SST)
  mysql_user: 
    state: present
    user: "{{ galera_dbusers['xtrabackup']['username'] }}"
    host: "localhost"
    password: "{{ galera_dbusers['xtrabackup']['password'] }}"
    priv: "*.*:grant,process,lock tables,reload,replication client"
    login_user: root
    login_password: "{{galera_dbusers['root']['password']}}"
